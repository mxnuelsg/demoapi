//
//  TweetCell.swift
//  WizeTwitter
//
//  Created by Manuel Salinas on 7/9/18.
//  Copyright © 2018 Manuel Salinas. All rights reserved.
//

import UIKit

class TweetCell: UITableViewCell
{
    //MARK: VARIABLES & OUTLETS
    @IBOutlet weak fileprivate var ivPicture: UIImageView!
    @IBOutlet weak fileprivate var lblUsername: UILabel!
    @IBOutlet weak fileprivate var lblInfo: UILabel!
    
    //MARK: LIFE CYCLE
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.loadConfig()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        self.ivPicture.cornerRadius()
    }
    
    //MARK: INFO
    func loadInfo(_ tweet: Tweet)
    {
        self.ivPicture.load(link: tweet.user.profilePicture)
        self.lblUsername.text = "@" + tweet.user.username
        self.lblInfo.text = tweet.text
    }
}
