//
//  DetailTweetController.swift
//  WizeTwitter
//
//  Created by Manuel Salinas on 7/9/18.
//  Copyright © 2018 Manuel Salinas. All rights reserved.
//

import UIKit

class DetailTweetController: UIViewController
{
    //MARK: VARIABLES & OUTLETS
    @IBOutlet weak fileprivate var lblName: UILabel!
    @IBOutlet weak fileprivate var ivProfile: UIImageView!
    @IBOutlet weak fileprivate var lblUsername: UILabel!
    @IBOutlet weak fileprivate var lblLocation: UILabel!
    @IBOutlet weak fileprivate var lblInfo: UILabel!
    
    var tweet: Tweet!
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonIcon()
        self.navigationController?.type(.dark)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
         self.navigationController?.type(.light)
    }
    
    deinit
    {
        print("Deinit: DetailTweetController")
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        self.title = "Tweet"
        self.ivProfile.setBorder()
        
        //Items
        let btnItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.add))
        navigationItem.rightBarButtonItem = btnItem
        
        self.loadInfo()
    }
    
    fileprivate func loadInfo()
    {
        self.ivProfile.load(link: self.tweet.user.profilePicture)
        self.lblName.text = self.tweet.user.name
        self.lblUsername.text = "@" + self.tweet.user.username
        self.lblLocation.text = self.tweet.user.location
        self.lblInfo.text = self.tweet.text
    }
    
    //MARK: ACTIONS
    @objc fileprivate func add()
    {
        
    }
}


