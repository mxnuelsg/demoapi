//
//  ViewController.swift
//  WizeTwitter
//
//  Created by Manuel Salinas on 7/9/18.
//  Copyright © 2018 Manuel Salinas. All rights reserved.
//

import UIKit

let getUser = "https://wizetwitterproxy.herokuapp.com/api/user"
let getTweets = "https://wizetwitterproxy.herokuapp.com/api/statuses/user_timeline"

class ViewController: UIViewController
{
    //MARK: VARIABLES & OUTLETS
    @IBOutlet weak fileprivate var ivBackground: UIImageView!
    @IBOutlet weak fileprivate var ivPicture: UIImageView!
    @IBOutlet weak fileprivate var lblUsername: UILabel!
    @IBOutlet weak fileprivate var lblDescription: UILabel!
    @IBOutlet weak var btnSettings: UIButton!
    
    fileprivate var vcTimeline: TimelineTableController!
    
    fileprivate var user: User! {
        didSet{
            self.loadInfo()
        }
    }
    
    fileprivate var tweets = [Tweet](){
        didSet{
            self.vcTimeline.tweets = self.tweets
        }
    }

    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonIcon()
    }
    
    deinit
    {
        print("Deinit: ViewController")
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        //UI
        self.ivBackground.blur()
        self.btnSettings.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2)
        self.btnSettings.cornerRadius(10)
        self.ivPicture.setRoundAndBorder()
        
        //Items
        let btnSearch = UIBarButtonItem(title: "Search", style: .plain, target: self, action: #selector(self.search))
        let btnCompose = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.compose))
        self.navigationItem.rightBarButtonItems = [btnCompose, btnSearch]
        
        //Get Data
        self.vcTimeline.showMessage("Loading...")
        self.requestData(url: getUser, completion: { [weak self] (user: User) in
            
            self?.user = user
        })
        
        self.requestData(url: getTweets, completion: { [weak self]  (tweets: [Tweet]) in
            
            self?.tweets = tweets
            self?.vcTimeline.dismissMessage()
        })
    }
    
    fileprivate func loadInfo()
    {
        self.ivBackground.load(link: self.user.profileBackground)
        self.ivPicture.load(link: self.user.profilePicture)
        
        DispatchQueue.main.async {
            
            self.lblUsername.text = self.user.username
            self.lblDescription.text  = self.user.description
        }
    }
    
    //MARK: WEB SERVICE
    fileprivate func requestData<T: Decodable>(url urlString: String, completion: @escaping (T) -> ())
    {
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!){ (data, response, error) in
            
            if let err = error {
                
                print(err.localizedDescription)
                return
            }
            
            guard let data = data else {
                
                print("Data is empty")
                return
            }
            
            do
            {
                let obj = try JSONDecoder().decode(T.self, from: data)
                completion(obj)
            }
            catch let jsonError
            {
                print("Failed to decode JSON:", jsonError)
            }
            }.resume()
    }
    
    //MARK: ACTIONS
    @objc func search()
    {
        //...
    }
    
    @objc func compose()
    {
        //...
    }
    
    @IBAction func openSettings()
    {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Help", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Sign Out", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let id = segue.identifier , id == "TimelineTableController" {
            
            self.vcTimeline = segue.destination as! TimelineTableController
        }
    }
}

