//
//  Tweet.swift
//  WizeTwitter
//
//  Created by Manuel Salinas on 7/11/18.
//  Copyright © 2018 Manuel Salinas. All rights reserved.
//

import Foundation

struct Tweet: Codable
{
    let id: Int
    let text: String
    let user: User
}
