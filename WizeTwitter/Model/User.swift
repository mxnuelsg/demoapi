//
//  Tweet.swift
//  WizeTwitter
//
//  Created by Manuel Salinas on 7/9/18.
//  Copyright © 2018 Manuel Salinas. All rights reserved.
//

import Foundation

struct User: Codable
{
    let id: Int
    let description: String
    let name: String
    let username: String
    let profileBackground: String
    let profilePicture: String
    let location: String
    
    private enum CodingKeys: String, CodingKey {
        
        case id
        case description
        case name
        case username = "screen_name"
        case profileBackground = "profile_banner_url"
        case profilePicture = "profile_image_url"
        case location

    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(Int.self, forKey: .id)
        description = try values.decode(String.self, forKey: .description)
        name = try values.decode(String.self, forKey: .name)
        username = try values.decode(String.self, forKey: .username)
        profileBackground = try values.decode(String.self, forKey: .profileBackground)
        profilePicture = try values.decode(String.self, forKey: .profilePicture)
        location = try values.decode(String.self, forKey: .location)
    }
}
