//
//  NavyInvisible.swift
//  WizeTwitter
//
//  Created by Manuel Salinas on 7/9/18.
//  Copyright © 2018 Manuel Salinas. All rights reserved.
//

import UIKit

enum NavType
{
    case dark
    case light
}

class NavyInvisible: UINavigationController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.tintColor = .white
        
        let navbarFont = UIFont.systemFont(ofSize: 13)
        self.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: navbarFont, NSAttributedStringKey.foregroundColor: UIColor.white]
    }
}

extension UINavigationController
{
    func type(_ t: NavType)
    {
        self.navigationBar.tintColor = t == .dark ? .black : .white
        
        let navbarFont = UIFont.systemFont(ofSize: 13)
        self.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: navbarFont, NSAttributedStringKey.foregroundColor: t == .dark ? UIColor.black : UIColor.white]
    }
}
