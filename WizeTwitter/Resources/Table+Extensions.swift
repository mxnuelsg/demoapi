//
//  UIKit.swift
//  WizeTwitter
//
//  Created by Manuel Salinas on 7/11/18.
//  Copyright © 2018 Manuel Salinas. All rights reserved.
//

import UIKit

extension UITableView
{
    func hideEmtpyCells()
    {
        self.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    func showMessage(_ message: String)
    {
        let lblMessage = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 63))
        lblMessage.textAlignment = .center
        lblMessage.font = UIFont.italicSystemFont(ofSize: 16)
        lblMessage.textColor = UIColor.darkGray
        lblMessage.text = message
        lblMessage.numberOfLines = 0
        
        self.backgroundView = lblMessage
    }
    
    func dismissMessage()
    {
        DispatchQueue.main.async {
            self.backgroundView = nil
        }
    }
}

extension UITableViewController
{
    func showMessage(_ message: String)
    {
        self.tableView.showMessage(message)
    }
    
    func dismissMessage()
    {
        self.tableView.dismissMessage()
    }
}
