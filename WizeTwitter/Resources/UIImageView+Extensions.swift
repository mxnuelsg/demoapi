//
//  UIKit.swift
//  WizeTwitter
//
//  Created by Manuel Salinas on 7/11/18.
//  Copyright © 2018 Manuel Salinas. All rights reserved.
//

import UIKit

extension UIImageView
{
    func load(link: String)
    {
        guard let url = URL(string: link) else { return }
        let session = URLSession(configuration: .default)
        
        //creating Task
        let getImageFromUrl = session.dataTask(with: url) { (data, response, error) in
            
            if let e = error {
                
                print("Error Occurred: \(e)")
            }
            else
            {
                if (response as? HTTPURLResponse) != nil {
                    
                    //Image validation
                    if let imageData = data {
                        
                        let image = UIImage(data: imageData)
                        
                        DispatchQueue.main.async {
                            self.image = image
                        }
                        
                    }
                    else
                    {
                        print("Image file is currupted")
                    }
                }
                else
                {
                    print("No response from server")
                }
            }
        }
        getImageFromUrl.resume()
    }
}
