//
//  UIKit.swift
//  WizeTwitter
//
//  Created by Manuel Salinas on 7/11/18.
//  Copyright © 2018 Manuel Salinas. All rights reserved.
//

import UIKit

extension UIView
{
    func cornerRadius()
    {
        self.layer.cornerRadius = 3
        self.clipsToBounds = true
    }
    
    func cornerRadius(_ radius: CGFloat)
    {
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
    
    func blur()
    {
        let visuaEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        visuaEffectView.frame = bounds
        visuaEffectView.autoresizingMask = [.flexibleWidth , .flexibleHeight]
        visuaEffectView.translatesAutoresizingMaskIntoConstraints = true
        addSubview(visuaEffectView)
    }
    
    func round()
    {
        let half = self.frame.width / 2
        self.layer.cornerRadius = half
        self.clipsToBounds = true
    }
    
    func setBorder()
    {
        self.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        self.layer.borderWidth = 1
    }
    
    
    func setRoundAndBorder()
    {
        self.round()
        self.setBorder()
    }
}
