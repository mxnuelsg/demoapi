//
//  UIKit.swift
//  WizeTwitter
//
//  Created by Manuel Salinas on 7/11/18.
//  Copyright © 2018 Manuel Salinas. All rights reserved.
//

import UIKit

extension UIViewController
{
    static func getInstance<T: UIViewController>(in storyboard: String = "Main") -> T
    {
        let storyboard = UIStoryboard(name: storyboard, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "\(T.self)") as! T
    }
    
    func backButtonIcon(_ icon: UIImage = UIImage())
    {
        navigationItem.backBarButtonItem = UIBarButtonItem(image: icon, style: .plain, target: self, action: nil)
    }
}
